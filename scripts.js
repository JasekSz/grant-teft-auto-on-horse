var gunCoordinatesX = 1;
var gunCoordinatesY = 1;
var quantity = 10;
var enemyCoordinatesX = [];
var enemyCoordinatesY = [];
var bulletCoordinatesX = [];
var bulletCoordinatesY = [];
var score = 0;
var oldScore = 0;
var food = [];
var bullet = [];
var howManyBullets = 0;
var gunPosition = 270;

score = quantity;
oldScore = score;
$('#score').html('<p>pozostało jeszcze: ' + score+ ' kropek</p>');
$(window).keypress(function(e) {
    if(oldScore == score-1){
        oldScore--;
        
    }
    if(gunCoordinatesX>0 && gunCoordinatesY>0 && gunCoordinatesX<300 && gunCoordinatesY<300) {
        // ruszanie w prawo, klawisz "d"

        if (e.which === 100) {
            gunCoordinatesX = gunCoordinatesX + 5;
            movegunCoordinatesX(gunCoordinatesX);
            $("img").css("transform", "scaleX(-1)");
            gunPosition = 90;
        }
        // ruszanie w lewo, klawisz "a"
        if (e.which === 97) {
            gunCoordinatesX = gunCoordinatesX - 5;
            movegunCoordinatesX(gunCoordinatesX);
            $("img").css("transform", "scaleX(1)");
            gunPosition = 270;
        }
        // ruszanie w dół klawisz "s"
        if (e.which === 115) {
            gunCoordinatesY = gunCoordinatesY + 5;
            movegunCoordinatesY(gunCoordinatesY);
            $("img").css("transform", "rotate(-90deg)");
            gunPosition = 180;
        }
        // ruszanie w górę klawisz "w"
        if (e.which === 119) {
            gunCoordinatesY = gunCoordinatesY - 5;
            movegunCoordinatesY(gunCoordinatesY);
            $("img").css("transform", "rotate(90deg)");
            gunPosition = 0;
        }
        
    
        // szczelanie, klawisz "space"
        if (e.which === 32) {
        howManyBullets++;
           createBullet(howManyBullets);
            
        }
    } else {
        if(gunCoordinatesX<0) {
            gunCoordinatesX = gunCoordinatesX + 5;
        }
        if(gunCoordinatesX>300) {
            gunCoordinatesX = gunCoordinatesX - 5;
        }
        if(gunCoordinatesY>300) {
            gunCoordinatesY = gunCoordinatesY - 5;
        }
        if(gunCoordinatesY<0) {
            gunCoordinatesY = gunCoordinatesY + 5;
        }
    }
    
    // funkcja od sprzawdania spotkania guna i enemisa
//    for(let i = 0; i <= quantity; i++){
//        if (gunCoordinatesY < enemyCoordinatesY[i] && gunCoordinatesY > enemyCoordinatesY[i] - 50 && gunCoordinatesX < enemyCoordinatesX[i] && gunCoordinatesX > enemyCoordinatesX[i] - 50) {
//        alert("zjadlem klocek");
//            $('#c' + i).remove();    
//        }
//    }
        score = $("#board").children().length - 1;
        $('#score').html('<p>pozostało jeszcze: ' + score+ ' kropek</p>');


   // AnimateRotate(360);


    if(score==0){

        function AnimateRotate(angle) {
            // caching the object for performance reasons
            var $elem = $('#klocek');

            // we use a pseudo object for the animation
            // (starts from `0` to `angle`), you can name it as you want
            $({deg: 0}).animate({deg: angle}, {
                duration: 2000,
                step: function(now) {
                    // in the step-callback (that is fired each step of the animation),
                    // you can use the `now` paramter which contains the current
                    // animation-position (`0` up to `angle`)
                    $elem.css({
                        transform: 'rotate(' + now + 'deg)'
                    });
                }
            });
        }

    }
});

    
function getRandom() {
   return Math.floor(Math.random()*300);
}

// ruszanie Gunem

function movegunCoordinatesX(i){
    $('#klocek').css("left", i+"px");
}
function movegunCoordinatesY(i){
    $('#klocek').css("top", i+"px");
}

// robienie ememiesów

function createFood(quantity){
    for(let i = 0; i <= quantity; i++){
        var elementName = 'c' + i;
        $('#board').append('<div class="food" id="'+ elementName + '"</div>');
        food[i]=i;
    }
    for(let i = 0; i <= quantity; i++){
        var elementName = '#c' + i;
        var x = getRandom();
        var y = getRandom();
        if(x<50&&y<50){
            x=x+50; 
            y=y+50;
        }
        enemyCoordinatesX[i]=x;
        enemyCoordinatesY[i]=y;
        $(elementName).css("left", x + "px");
        $(elementName).css("top", y + "px");
    }

}
createFood(quantity);

function createBullet(i){
        var elementName1 = 'b' + i;
        $('#board').append('<div class="bullet" id="'+ elementName1 + '"</div>');
        var x = gunCoordinatesX + 20;
        var y = gunCoordinatesY + 20;
        var elementName = '#b' + i;
        $(elementName).css("left", x + "px");
        $(elementName).css("top", y + "px");
        bulletCoordinatesX[i-1]=x;
        bulletCoordinatesY[i-1]=y;
        setInterval(function(){
            var bulletX = document.getElementById(elementName1).style.left;
            bulletX = bulletX.slice(0, -2);
            bulletX = Math.round(bulletX);

            for(let i = 0; i <= quantity; i++){
            if (bulletCoordinatesY[i] >= enemyCoordinatesY[i] && bulletCoordinatesY[i] <= enemyCoordinatesY[i] + 20 && bulletCoordinatesX[i] >= enemyCoordinatesX[i] && bulletCoordinatesX[i] <= enemyCoordinatesX[i] + 20) {
                enemyCoordinatesX[i]=500;
                enemyCoordinatesY[i]=500;
                $('#b' + i).hide();     
                $('#c' + i).hide();  
                alert("zeszczelony");
               }; 
            }
        
            var bulletY = document.getElementById(elementName1).style.top;
            bulletY = bulletY.slice(0, -2);
            bulletY = Math.round(bulletY);
            
            bulletCoordinatesX[i-1]=bulletX;
            bulletCoordinatesY[i-1]=bulletY;
        }, 3);
    
        switch(gunPosition) {
    case 0:
        $(elementName).animate({ 
        top: "-=550px",
        }, 5000 );
        setInterval(function(){$(elementName).hide();}, 5000);
        break;
   case 90:
        $(elementName).animate({ 
        left: "+=550px",
        }, 5000 );
        setInterval(function(){$(elementName).hide();}, 5000);
        break;
   case 180:
        $(elementName).animate({ 
        top: "+=550px",
        }, 5000 );
        setInterval(function(){$(elementName).hide();}, 5000);       
        break;
   case 270:
        $(elementName).animate({ 
        left: "-=550px",
        }, 5000 );
        setInterval(function(){$(elementName).hide();}, 5000);       
        break;
                
  
}
    
//    setInterval(function(){   
//        for(let i = 0; i <= quantity; i++){
//            if (bulletCoordinatesY[i] >= enemyCoordinatesY[i] && bulletCoordinatesY[i] <= enemyCoordinatesY[i] + 20 && bulletCoordinatesX[i] >= enemyCoordinatesX[i] && bulletCoordinatesX[i] <= enemyCoordinatesX[i] + 20) {
//                enemyCoordinatesX[i]=500;
//                enemyCoordinatesY[i]=500;
//                $('#b' + i).hide();     
//                $('#c' + i).hide();  
//                alert("zeszczelony");
//            }; 
//        }
//        }, 100);
       
}

//k
//    for(let i = 0; i <= quantity; i++){
//        if (bulletCoordinatesY < enemyCoordinatesY[i] && gunCoordinatesY > enemyCoordinatesY[i] - 50 && gunCoordinatesX < enemyCoordinatesX[i] && gunCoordinatesX > enemyCoordinatesX[i] - 50) {
//        alert("zjadlem klocek");
//            $('#c' + i).remove();    
//        }
//    }

 //55 > 50 && 55 > 0 && 55 > 50 && 55 > 0


